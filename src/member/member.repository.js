const prisma = require("../db");

const findMembers = async () => {
  const members = await prisma.member.findMany();
  return members;
};

const findMemberByCode = async (code) => {
  const member = await prisma.member.findUnique({
      where: {code}
  });

  return member;
};

const insertMember = async (memberData) => {
  const member = await prisma.member.create({
    data:{
        code: memberData.code,
        name: memberData.name
    }
  });

  return member;
};

const deleteMember = async (code) => {
    await prisma.member.delete({
        where: {code}
    });
};

const editMember = async (code, memberData) => {
    const member = await prisma.member.update({
       where: {code},
       data: {
           name: memberData.name
       }
    });

    return member;
}

module.exports = {
  findMembers,
  findMemberByCode,
  insertMember,
    deleteMember,
    editMember
};