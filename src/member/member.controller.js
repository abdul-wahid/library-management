const express = require("express");
const prisma = require("../db");

const {
    editMemberByCode,
    getMemberByCode,
    deleteMemberByCode,
    createMember,
    getAllMembers
} = require("./member.service");

const router = express.Router();

router.get("/", async (req, res) => {
    const members = await getAllMembers();
    res.send({
        data: members
    });
});

router.get("/:code", async (req, res) => {
   try{
       const memberCode = req.params.code;
       const member = await getMemberByCode(memberCode);
       res.send({
           data: member
       });
   }catch (err){
       res.status(400).send(err.message);
   }
});

router.post("/", async (req, res) => {
   try {
       const newMemberData = req.body;
       const member = await createMember(newMemberData);
       res.send({
          data: member,
          message: "Create member success"
       });
   }catch (err){
       res.status(400).send(err.message);
   }
});

router.delete("/:code", async (req, res) => {
   try {
       const memberCode = req.params.code;
       await deleteMemberByCode(memberCode);
       res.send("Member deleted");
   } catch (err){
       res.status(400).send(err.message);
   }
});

router.put("/:code", async (req, res) => {
   const memberCode = req.params.code;
   const memberData = req.body;

   if(!memberData.name){
       return res.status(400).send("Some fields are missing");
   }

   const member = await editMemberByCode(memberCode,memberData);
   res.send({
      data: member,
      message: "Edit member success"
   });
});

module.exports = router;