const prisma = require("../db");
const {
    editMember,
    deleteMember,
    insertMember,
    findMemberByCode,
    findMembers
} = require("./member.repository");

const {
    getBorrowsByMemberCodeReturnDate
} = require("../common/common.service");

const getAllMembers = async () => {
    const members = await findMembers();

    const result = await Promise.all(
        members.map(async (member) => {
           const borrowedBooks = await getBorrowsByMemberCodeReturnDate(member.code);

            return {
                ...member,
                borrowedBooks: borrowedBooks.length
            };
        })
    );

    return result;
};

const getMemberByCode = async (code) => {
    const member = await findMemberByCode(code);

    if(!member) throw Error("Member not found");

    return member;
}

const createMember = async (newMemberData) => {
    const member = await insertMember(newMemberData);
    return member;
};

const deleteMemberByCode = async (code) => {
    await getMemberByCode(code);
    await deleteMember(code);
}

const editMemberByCode = async (code, memberData) => {
  await getMemberByCode(code);
  const member = await editMember(code, memberData);
  return member;
};

module.exports = {
  getAllMembers,
    getMemberByCode,
    createMember,
    deleteMemberByCode,
    editMemberByCode
};