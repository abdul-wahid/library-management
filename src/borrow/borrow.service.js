const prisma = require("../db");
const {
    findBorrowByMemberCodeBookCodeReturnDate,
    findBorrowsByMemberCodeReturnDate,
    insertBorrow,
    editBorrow,
    findBorrows
} = require("./borrow.repository");

const {
    getMemberByCode,
    editMemberByCode
} = require("../member/member.service");

const {
    getBookByCode,
    editBookByCode
} = require("../book/book.service");

const getBorrows = async () => {
    const borrows = await findBorrows();
    return borrows;
}

const getBorrowsByMemberCodeReturnDate = async (memberCode) => {
  const borrows = await findBorrowsByMemberCodeReturnDate(memberCode);
  return borrows;
};

const borrowBook = async (memberCode, bookCode) => {
    const member = await getMemberByCode(memberCode);

    if(member.penaltyEndData && new Date() < new Date(member.penaltyEndData)) throw Error('Member is currently penalized');

    const borrows = await findBorrowsByMemberCodeReturnDate(memberCode);

    if(borrows.length >= 2) throw Error('Member cannot borrow more than 2 books');

    const book = await getBookByCode(bookCode);

    if(book.stock === 0) throw Error("Book not available");

    book.stock = book.stock - 1;
    await editBookByCode(book.code, book);

    return insertBorrow({memberCode, bookCode});
}

const returnBook = async (memberCode, bookCode) => {
    const borrow = await findBorrowByMemberCodeBookCodeReturnDate(memberCode, bookCode);

    if(!borrow) throw Error('Borrow record not found');

    const member = await getMemberByCode(memberCode);

    const borrowDate = new Date(borrow.borrowDate);
    const returnDate = new Date();

    const book = await getBookByCode(bookCode);
    book.stock = book.stock + 1;

    await editBookByCode(book.code, book);

    if ((returnDate - borrowDate) / (1000 * 60 * 60 * 24) > 7) {
        const penaltyEndDate = new Date();
        penaltyEndDate.setDate(returnDate.getDate() + 3);

        member.penaltyEndData = penaltyEndDate;
        await editMemberByCode(member.memberCode, member)

        throw Error('Book returned late. Member is penalized');
    }

    return editBorrow(borrow.id, returnDate);
};

module.exports = {
  borrowBook,
    returnBook,
    getBorrowsByMemberCodeReturnDate,
    getBorrows
};