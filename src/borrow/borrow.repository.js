const prisma = require("../db");

// mencari pinjaman berdasarkan memberCode dan returnDate null
const findBorrowsByMemberCodeReturnDate = async (memberCode) => {
    const borrows = await prisma.borrow.findMany({
       where: {memberCode, returnDate: null}
    });
    return borrows;
}

const findBorrows = async () => {
    const borrows = await prisma.borrow.findMany();
    return borrows;
}

// buat peminjaman
const insertBorrow = async (borrowData) => {
  const borrow = await prisma.borrow.create({
      data: {
          memberCode: borrowData.memberCode,
          bookCode: borrowData.bookCode
      }
  });

  return borrow;
};

// mencari pinjaman berdasarkan memberCode, book dan returnDate is null
const findBorrowByMemberCodeBookCodeReturnDate = async (memberCode, bookCode) => {
    const borrow = await prisma.borrow.findFirst({
       where: {memberCode, bookCode, returnDate: null}
    });

    return borrow;
}

// update pinjaman
const editBorrow = async (id, returnDate) => {
    const borrow = await prisma.borrow.update({
        where: {id},
        data: {returnDate}
    })

    return borrow;
}

module.exports = {
  findBorrowsByMemberCodeReturnDate,
    insertBorrow,
    findBorrowByMemberCodeBookCodeReturnDate,
    editBorrow,
    findBorrows
};