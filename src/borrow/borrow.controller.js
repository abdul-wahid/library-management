const express = require("express");
const prisma = require("../db");

const {
    borrowBook,
    returnBook,
    getBorrows
} = require("./borrow.service");

const router = express.Router();

router.get("/", async (req, res) => {
    const borrows = await getBorrows();
    res.send({
        data: borrows
    });
});

router.post("/", async (req, res) => {
   try {
       const newBorrow = req.body;
       const borrow = await borrowBook(newBorrow.memberCode, newBorrow.bookCode);
       res.send({
          data: borrow,
          message: "Borrow book success"
       });
   } catch (err){
       res.status(400).send(err.message);
   }
});

router.post("/return-book", async (req, res) => {
   try {
       const reqReturn = req.body;
       const borrow = await returnBook(reqReturn.memberCode, reqReturn.bookCode);
       res.send({
          data: borrow,
          message: "Return book success"
       });
   } catch (err){
       res.status(400).send(err.message);
   }
});

module.exports = router;