const {findBorrowsByMemberCodeReturnDate} = require("../borrow/borrow.repository");
const getBorrowsByMemberCodeReturnDate = async (memberCode) => {
    const borrows = await findBorrowsByMemberCodeReturnDate(memberCode);
    return borrows;
};

module.exports = {
  getBorrowsByMemberCodeReturnDate
};