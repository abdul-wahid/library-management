const express = require("express");
const dotenv = require("dotenv");
const app = express();

dotenv.config();
const PORT = process.env.PORT;

app.use(express.json());

app.listen(PORT, () => {
    console.log(`Library API running in port: ${PORT}`);
});

app.get("/library-api", (req, res) => {
    res.send("Wellcome To API Library");
});

const bookController = require("./book/book.controller");
app.use("/book", bookController);

const memberController = require("./member/member.controller");
app.use("/member", memberController);

const borrowController = require("./borrow/borrow.controller");
app.use("/borrow", borrowController);

const swaggerUi = require("swagger-ui-express");
const apiDoc = require("./apidocs.json");
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(apiDoc));


