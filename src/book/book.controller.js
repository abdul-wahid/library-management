const express = require("express");
const prisma = require("../db");

const {
    getAllBooks,
    getBookByCode,
    createBook,
    editBookByCode,
    deleteBookByCode
} = require("./book.service");

const router = express.Router();

router.get("/", async (req, res) => {
   const books = await getAllBooks();
   res.send({
       data: books
   });
});

router.get("/:code", async (req, res) => {
   try {
       const bookCode = req.params.code;
       const book = await getBookByCode(bookCode);
       res.send({
           data: book
       });
   }catch (err){
       res.status(400).send(err.message);
   }
});

router.post("/", async (req, res) => {
    try {
       const newBookData = req.body;
       const book = await createBook(newBookData);
       res.send({
          data: book,
          message: "Create book success"
       });
   }catch (err){
       res.status(400).send(err.message);
   }
});

router.delete("/:code", async (req, res) => {
   try{
       const bookCode = req.params.code;
       await deleteBookByCode(bookCode);
       res.send("Book deleted");
   } catch (err){
       res.status(400).send(err.message);
   }
});

router.put("/:code", async (req, res) => {
   const bookCode = req.params.code;
   const bookData = req.body;
   if(
       !(
           bookData.author &&
           bookData.stock &&
           bookData.title
       )
   ){
       return res.status(400).send("Some fields are missing");
   }

   const book = await editBookByCode(bookCode, bookData);
   res.send({
       data: book,
       message: "Edit book success"
   });
});

module.exports = router;