const prisma = require("../db");

const findBooks = async () => {
    const books = await prisma.book.findMany();
    return books;
};

const findBookByCode = async (code) => {
  const book = await prisma.book.findUnique({
     where:{code}
  });
  return book;
};

const insertBook = async (bookData) => {
  const book = await prisma.book.create({
      data: {
          code: bookData.code,
          author: bookData.author,
          stock: bookData.stock,
          title: bookData.title
      }
  });
  return book;
};

const deleteBook = async (code) => {
    await prisma.book.delete({
        where: {code}
    });
};

const editBook = async (code, bookData) => {
  const book = await prisma.book.update({
    where: {code},
    data: {
        author: bookData.author,
        stock: bookData.stock,
        title: bookData.title
    }
  });

  return book;
};

module.exports = {
  findBooks,
  findBookByCode,
  insertBook,
  deleteBook,
  editBook
};