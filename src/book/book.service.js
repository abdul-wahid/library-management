const prisma = require("../db");
const {
    findBooks,
    findBookByCode,
    insertBook,
    editBook,
    deleteBook
} = require("./book.repository");

const getAllBooks = async () => {
    const books = await findBooks();
    return books;
}

const getBookByCode = async (code) => {
  const book = await findBookByCode(code);
  if(!book) throw Error("Book not found");
  return book;
};

const createBook = async (newBookData) => {
  const book = await insertBook(newBookData);
  return book;
};

const deleteBookByCode = async (code) => {
  await getBookByCode(code);
  await deleteBook(code);
};

const editBookByCode = async (code, bookData) => {
    await getBookByCode(code);
    const book = await editBook(code, bookData);
    return book;
}

module.exports = {
  getAllBooks,
    getBookByCode,
    createBook,
    deleteBookByCode,
    editBookByCode
};